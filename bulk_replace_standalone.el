;;; Bulk replacement of multiple strings (like `abbrev`), including embedded delimiters (like spaces, unlike `abbrev`).
;;; See related posts:
;;; * http://lists.gnu.org/archive/html/help-gnu-emacs/2015-12/msg00077.html (TLR initial query)
;;; * http://lists.gnu.org/archive/html/help-gnu-emacs/2015-12/msg00079.html (PJB response, from which initial `bulk-replace-region` code was adapted)
;;; * http://emacs.stackexchange.com/q/19053/5444 (my emacs-SE question)
;;; ** http://emacs.stackexchange.com/a/19059/5444 , only last bit of answer

;;; Path to file containing elisp sexp's containing {to-replace, replace-with} tuples.
;;; Form given in http://lists.gnu.org/archive/html/help-gnu-emacs/2015-12/msg00079.html
;;; for testing
;; following failed when working on files/buffers outside of this repo
; (defconst BULK-REPLACE-TUPLES-FILEPATH "./sample_replacements.sexp")
;; following works
(defconst BULK-REPLACE-TUPLES-FILEPATH (expand-file-name "./sample_replacements.sexp"))
;;; TODO: test file is readable!
;; following for use in normal emacs setup, if `LOCAL-EMACS-DIR` defined above in init.el
;(defconst BULK-REPLACE-TUPLES-FILEPATH (expand-file-name (concat LOCAL-EMACS-DIR "/tlr_bulk_replacements.sexp")))

;;; How to prompt for filepaths in `bulk-replace-file`*?
;;; TODO: support internationalization, customization.
(defconst BULK-REPLACE-FILEPATH-PROMPT "Path to file to bulk-replace: ")
;;; Note: unfortunately use of this breaks "normal" use of `(interactive "F")`: see http://emacs.stackexchange.com/a/19083/5444

;;; `with-file` extracted (until I manage dependencies better!) from https://raw.githubusercontent.com/informatimago/emacs/master/pjb-emacs.el

(defmacro* with-file (file-and-options &body body)
  "
find-file or find-file-literally, process body, and optionally save the buffer
and kill it.
save is not done if body exits with exception.
kill is always done as specified.
FILE-AND-OPTION: either an atom evaluated to a path,
                 or (path &key (save t) (kill t) (literal nil))
"
  (if (atom file-and-options)
      `(with-file (,file-and-options) ,@body)
      ;; destructuring-bind is broken, we cannot give anything else than nil
      ;; as default values:
      
      (destructuring-bind (path &key (save nil savep) (kill nil killp)
                                  (literal nil literalp))
          file-and-options
        (unless savep (setf save t))
        (unless killp (setf kill t))
        `(save-excursion
          (unwind-protect
               (progn
                 (,(if literal 'find-file-literally 'find-file) ,path)
                 (prog1 (save-excursion ,@body)
                   ,(when save `(save-buffer 1))))
            ,(when kill
               `(kill-buffer (current-buffer))))))))

;;; `replace-multiple-strings` extracted (until I manage dependencies better!) from https://raw.githubusercontent.com/informatimago/emacs/master/pjb-emacs.el

(defun replace-multiple-strings-in-current-buffer (replacements-alist)
  "Replaces all occurences of the keys in `replacements-alist' by their corresponding value.
The search is performed in sequentially once from (point) to (point-max)."
  (let ((re (concat "\\("
                    (mapconcat (lambda (entry) (regexp-quote (car entry)))
                               replacements-alist
                               "\\|")
                    "\\)")))
    (while (re-search-forward re (point-max) t)
      (let* ((key (buffer-substring-no-properties (match-beginning 1)
                                                  (match-end 1)))
             (val (cdr (assoc* key replacements-alist
                               :test (function string=)))))
        (if val
            (progn
              (delete-region (match-beginning 1) (match-end 1))
              (insert val)
              (goto-char (+ (match-beginning 1) (length val))))
            (goto-char (match-end 1)))))))

;;; Do bulk replacement! The following non-interactives are wrapped by several interactives/commands following.

(defun bulk-replace-region-non-interactive (start finish)
  "Replace all matching tuples in region non-(interactive)-ly. To be wrapped by other (interactive) functions."
;  (message "tuples filepath='%s'" BULK-REPLACE-TUPLES-FILEPATH) ; debugging
  (save-excursion
    (narrow-to-region start finish)
    (goto-char (point-min))
    (replace-multiple-strings-in-current-buffer
      (with-file BULK-REPLACE-TUPLES-FILEPATH
        (goto-char (point-min)) ; in case the tuples file is already open
        (read (current-buffer))
      )
    )
    (widen)
  )
)

(defun bulk-replace-buffer-non-interactive ()
  "Replace all matching tuples in current buffer non-(interactive)-ly. To be wrapped by other (interactive) functions."
  (bulk-replace-region-non-interactive (point-min) (point-max))
)

(defun bulk-replace-file-non-interactive (filepath)
  "Replace all matching tuples in current file @ filepath non-(interactive)-ly. To be wrapped by other (interactive) functions."
  ; adapted from https://www.gnu.org/software/emacs/manual/html_node/eintr/Find-a-File.html
  (set-buffer (find-file-noselect filepath))
  (bulk-replace-buffer-non-interactive)
  (save-buffer)
  (kill-buffer)
)

;;; The following interactives/commands work, but require the user to know more API.

; need definitions={begin, end} here to be filled-in by `(interactive "r")`
(defun bulk-replace-region (begin end)
  "Replace all matching tuples in region (interactive)-ly, wrapping `bulk-replace-region-non-interactive`."
  (interactive "r") ; need "r" to get region begin and end
  (bulk-replace-region-non-interactive begin end) 
)

(defun bulk-replace-buffer ()
  "Replace all matching tuples in current-buffer (interactive)-ly, wrapping `bulk-replace-region-non-interactive`."
  (interactive) ; purely to enable `M-x`
  (bulk-replace-buffer-non-interactive)
)

; (defun bulk-replace-file (filepath)
; ; Following works, but cannot be parameterized directly? See http://emacs.stackexchange.com/questions/19082/how-to-parameterize-prompt-for-interactive-command
; ;  (interactive "FPath to file to bulk-replace: ")
; ; Following fails, as detailed @ link above  
; ;  (interactive (concat "F" BULK-REPLACE-FILEPATH-PROMPT))
; )

(defun bulk-replace-file ()
  "Replace all matching tuples in file (interactive)-ly, wrapping `bulk-replace-region-non-interactive`."
  (interactive) ; purely to enable `M-x`
  (bulk-replace-file-non-interactive
    ; `(read-file-name)` does the prompting
    (expand-file-name (read-file-name BULK-REPLACE-FILEPATH-PROMPT))
  ) 
)

;;; This interactive/command similarly wraps the non-interactives but allows the user to know less API.
(defun bulk-replace (prefix-argument)
  " (ASSERT) satisfies the following usecases, evaluated in the following order:
* called with any prefix argument: bulk-replace file @ arg (which must be a filepath)
* called with region: bulk-replace region
* else/called with buffer: bulk-replace buffer
"
  (interactive "P")  ; to catch prefix argument (if given)
  (cond
    (prefix-argument ; got one, so prompt user for filepath
      ; note following works: does NOT require `(call-interactively 'bulk-replace-file)`
      (bulk-replace-file)
    )
    ((use-region-p)  ; we have a region, so bulk-replace-region
      (bulk-replace-region-non-interactive (region-beginning) (region-end))
    )
    (t               ; we always have a current-buffer, so bulk-replace-buffer
      (bulk-replace-buffer-non-interactive)
    )
  )
)
