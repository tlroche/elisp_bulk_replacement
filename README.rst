.. contents:: **Table of Contents**

summary
=======
              
Enables *bulk replacement* of string tuples in an `Emacs <https://en.wikipedia.org/wiki/Emacs>`_ buffer: see

1. `usecase <http://lists.gnu.org/archive/html/help-gnu-emacs/2015-12/msg00077.html>`_
#. `initial implementation <http://lists.gnu.org/archive/html/help-gnu-emacs/2015-12/msg00079.html>`_
#. sample `file <./sample_replacements.sexp>`_ containing {``to-replace``, ``replace-with``} tuples as `elisp <https://en.wikipedia.org/wiki/Emacs_Lisp>`_  `sexp <https://en.wikipedia.org/wiki/S-expression>`_\s
#. `sample input`_ containing ``to-replace`` text to be bulk-replaced (interspersed with other text). Note this text
    * is Unicode
    * displays properly in an Emacs buffer
    * displays properly with `Bitbucket's source renderer <./sample_input.txt>`_
    * displays improperly with `Bitbucket's raw renderer <../../raw/HEAD/sample_input.txt>`_
#. `test code`_ containing code currently loaded with ``eval-region``
#. design decisions discussed in `this emacs-SE question <http://emacs.stackexchange.com/questions/19053/elisp-pattern-for-handling-region-buffer-file>`_.
   
.. _sample input: ./sample_input.txt
.. _sample code: ./bulk_replace_standalone.el
.. _test code: ./bulk_replace_standalone.el

testing
=======

procedure
---------

Currently I'm testing (possibly incorrectly--your corrections are appreciated!) with the following procedure:

1. Load the `sample input`_ in one buffer (``test``).
2. Load the `test code`_ in another buffer (``code``).
3. ``eval`` the elisp in the code buffer (``M-x eval-buffer``).
4. Switch to the test buffer.
5. With the entire test buffer selected (``C-x h``),
    1. run ``M-x bulk-replace-region``
    #. undo the changes (``C-x u``)
6. With a proper subregion selected,
    1. run ``M-x bulk-replace-region``
    #. undo the changes
7. With no region selected (i.e., ``Mark deactivated``),
    1. run ``M-x bulk-replace-buffer``
    #. undo the changes
8. Whether region is selected or not,
    1. run ``M-x bulk-replace-file``
    #. give path to the sample input at the prompt
    #. verify the changes
    #. undo the changes (e.g., ``cp ./sample_input~ ./sample_input``)
9. Kill the test buffer.
10. In any other buffer, whether region is selected or not,

    1. run ``C-u M-x bulk-replace``
    #. verify the changes (to `sample input`_)
    #. undo the changes

    * Ideally the user could do ``C-u path-to-input M-x bulk-replace-file``, but emacs apparently `cannot take a non-integer prefix argument <https://www.gnu.org/software/emacs/manual/html_node/elisp/Prefix-Command-Arguments.html>`_. (Dunno why I thought this: I've been using Emacs forever.)

11. Reload the `sample input`_ in the test buffer.
12. With no region selected,

    1. run ``M-x bulk-replace``
    #. undo the changes

13. With a proper subregion selected,

    1. run ``M-x bulk-replace``
    #. undo the changes

14. With the entire test buffer selected,

    1. run ``M-x bulk-replace``
    #. undo the changes
     
status
------

Currently

1. Steps 1-4 above work without error.
#. Step#=5 works without error: after I call ``M-x bulk-replace-region``, the contents of the sample-input buffer are replaced with the contents of `sample output`_.
#. Step#=6 works without error: after I call ``M-x bulk-replace-region``, the contents of the region are replaced with the contents from the matching region of `sample output`_.
#. Step#=7 works without error: after I call ``M-x bulk-replace-buffer`` with no region, the contents of the buffer are replaced with the contents of `sample output`_.
#. Step#=8 works without error: after I call ``M-x bulk-replace-file``,
    1. I get prompted for a path. Provided I give the path to `sample input`_, ...
    #. ... results match those of step#=7.
#. Step#=9 works without error.
#. Step#=10 works without error: after I call ``C-u M-x bulk-replace``, results match those of step#=7.
#. Step#=11 works without error.
#. Step#=12 works without error: after I call ``M-x bulk-replace``: results match those of step#=7.
#. Step#=13 works without error: after I call ``M-x bulk-replace``: results match those of step#=6.
#. Step#=14 works without error: after I call ``M-x bulk-replace``: results match those of step#=5.
   
.. _sample output: ./sample_input.txt

TODOs
=====

Note some items already in this project's `issue tracker`_.

1.  Move all following TODOs to `issue tracker`_.
#.  Depend on `pjb-emacs.el`_ rather than just cut'n'pasting into `test code`_.
#.  Improve testing:

    #.  Test on files outside repo dir: this was how I discovered I needed to ``expand-file-name`` before setting ``BULK-REPLACE-TUPLES-FILEPATH``.
    #.  Automate testing.
    #.  Test from freshly-started emacs.

#.  Make more integrable into user's ``init.el`` or similar:

    1. crudely (and test!)
    #. package: make separate, or integrate into other package (e.g., `pjb-emacs.el`_ or whatever its package is)

.. _pjb-emacs.el: https://github.com/informatimago/emacs/blob/master/pjb-emacs.el
.. _issue tracker: ../../issues
